﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2Z2
{
    class Die
    {
        private int numberOfSides;
        private int randomGenerator;
        public Die(int numberOfSides, int randomGenerator)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = randomGenerator;
        }
        public int Roll()
        {
            return randomGenerator;
        }
    }
}
