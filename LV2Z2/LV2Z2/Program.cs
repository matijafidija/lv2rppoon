﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2Z2
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            DiceRoller DRoll = new DiceRoller();
            Die[] Dice = new Die[20];
            for (int i = 0; i < 20; i++)
            {
                Dice[i] = new Die(6, rand.Next(1, 7));
                DRoll.InsertDie(Dice[i]);
            }
            DRoll.RollAllDice();
            foreach (int roll in DRoll.resultForEachRoll)
            {
                Console.WriteLine(roll);
            }
        }
    }
}
